import os
import sys

rootdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path = [rootdir] + sys.path

from determinize import hash


def test_hash_simple():
    d1 = {'aaa': 'foo', 'bbb': 'bar'}
    d1_r = '37077762b59d0744ed5d91e0647961b9e44c9de9607fb22539f09a658f116847'
    assert hash.hash(d1) == d1_r


def test_hash_nested():
    d2 = {'aaa': {'ddd': 'foo', 'ccc': 5}, 'zzz': True}
    d2_r = 'f8931b4bdfdd99944db1de97beb933999d016e726a3ef1ae54bc689312fe49e8'
    d3 = {1: 10, 5: d2, 'a': [10, 20, True]}
    d3_r = '222ac65eeba4955848e420170c6cd9760d022955cc75446d304babac5e06329c'
    assert hash.hash(d2) == d2_r
    assert hash.hash(d3) == d3_r


def test_hash_set():
    d4 = set([10, 9, 'aaa', 'bbb'])
    d4_r = 'f40a2f1e04efa55f88e6a212f41c4271449c3e04612d7b6dfcc86c6e017a761a'
    d5 = set([10, 'aaa', (2, 26, 84), 'zeta'])
    d5_r = 'cc47de90ac3ccc200aa6e9cd1307a285d763e93b7f1e36c74f25e209a9f19576'
    assert hash.hash(d4) == d4_r
    assert hash.hash(d5) == d5_r
