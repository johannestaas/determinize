import sys
import os
from setuptools import setup

# determinize
# Make deterministic values from unordered or unsequenced types


def version_or_lt(s):
    version = [int(x) for x in s.split('.')]
    # Turns '2.6' into 206
    version = version[0] * 100 + version[1]
    sys_version = sys.version_info[0] * 100 + sys.version_info[1]
    return sys_version <= version


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="determinize",
    version="0.0.1",
    description="Make deterministic values from unordered or unsequenced types",
    author="Johan Nestaas",
    author_email="johannestaas@gmail.com",
    license="GPLv3+",
    keywords="",
    url="https://www.bitbucket.org/johannestaas/determinize",
    packages=['determinize'],
    package_dir={'determinize': 'determinize'},
    long_description=read('README.rst'),
    classifiers=[
        # 'Development Status :: 1 - Planning',
        # 'Development Status :: 2 - Pre-Alpha',
        'Development Status :: 3 - Alpha',
        # 'Development Status :: 4 - Beta',
        # 'Development Status :: 5 - Production/Stable',
        # 'Development Status :: 6 - Mature',
        # 'Development Status :: 7 - Inactive',
        'License :: OSI Approved :: GNU General Public License v3 or later '
        '(GPLv3+)',
        'Environment :: Console',
        'Environment :: X11 Applications :: Qt',
        'Environment :: MacOS X',
        'Environment :: Win32 (MS Windows)',
        'Operating System :: POSIX',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python',
    ],
    install_requires=[
        lib for version, lib in [
            ('2.6', 'ordereddict'),
        ]
        if version_or_lt(version)
    ],
    # entry_points={
    #     'console_scripts': [
    #         'determinize=determinize:main',
    #     ],
    # },
    # package_data={
    #     'determinize': ['catalog/*.edb'],
    # },
    # include_package_data=True,
)
