'''
determinize.hash

Hashes data with unordered elements. Brings order where there is disorder.
'''
import sys
import json
from hashlib import sha256
from datetime import datetime, date

VERSION = sys.version_info
PY26, PY27, PY3x = False, False,False
if VERSION[0] == 2:
    if VERSION[1] == 6:
        PY26 = True
    elif VERSION[1] == 7:
        PY27 = True
    else:
        sys.stderr.write('Python version is {0}.{1}, may be incompatible.\n'
                         .format(*VERSION))
elif VERSION[0] == 3:
    PY3x = True
else:
    sys.stderr.write('Version is {}, may be incompatible.\n'.format(VERSION))


if PY26:
    from ordereddict import OrderedDict
else:
    from collections import OrderedDict


def _compatible_cmp(a):
    return json.dumps(a)


def ordered(data):
    if isinstance(data, dict):
        new = []
        for key, val in data.items():
            new += [(key, ordered(val))]
        return sorted(new, key=_compatible_cmp)
    elif isinstance(data, list):
        new = []
        for item in data:
            new += [ordered(item)]
        return new
    elif isinstance(data, tuple):
        new = []
        for item in data:
            new += [ordered(item)]
        return tuple(new)
    elif isinstance(data, set):
        new = []
        for item in data:
            new += [ordered(item)]
        return sorted(new, key=_compatible_cmp)
    elif not PY26 and isinstance(data, OrderedDict):
        return new.items()
    elif isinstance(data, (datetime, date)):
        return data.isoformat()
    else:
        return data


def deterministic_json(data, indent=None):
    ordered_data = ordered(data)
    return json.dumps(ordered_data, indent=indent)


def hash(data):
    json_str = deterministic_json(data)
    if PY3x:
        json_str = json_str.encode('utf-8')
    return sha256(json_str).hexdigest()
