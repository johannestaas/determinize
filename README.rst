determinize
===========

Make deterministic values from unordered or unsequenced types, compatible across Python versions 2.6, 2.7 and 3.x.
It is used to create a hash from something like a nested dictionary with embedded sets and ordereddicts, however you like. All environments should create the same SHA256 hash.

This is useful to create a signature from a complex document or object.

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Determinize is an API to use programmatically. To use it, call `determinize.hash`'s `hash` function::

    from determinize.hash import hash
    data = {
        'foo': 'bar',
        'quux': {
            1,
            2,
            (
                5, 7, (('a', 'b'),),
            )
        }
    }
    result = hash(data)
    # result: 506ff1f2c303dcf868b8d30fa1e7be5242e11f866e31cc6a82f3520519bd7a3a

Release Notes
-------------

:0.0.1:
    Project created
